import sqlite3
import os
import time
import io

DATABASE_PATH = os.path.join('data', 'entreprises.db')

def check_if_exists(id_company):

    connexion = sqlite3.connect(DATABASE_PATH)
    cursor = connexion.cursor() 

    data = (id_company, )
    cursor.execute('SELECT id FROM entreprises WHERE id=?', data)
    
    result = cursor.fetchone()

    connexion.close()

    if result is None:
        return False
    return True


def insert_company(data):

    connexion = sqlite3.connect(DATABASE_PATH)
    cursor = connexion.cursor() 

    columns = ', '.join(data.keys())
    placeholders = ', :'.join(data.keys())
    placeholders = ':' + placeholders
    sql = 'INSERT INTO entreprises ({}) VALUES ({})'.format(columns, placeholders)
    
    #print('Inserting for company : {}'.format(data['id']))
    cursor.executemany(sql, [data])

    connexion.commit()
    connexion.close()

def export_database(delimiter=','):

    lines = []
    filename= 'export_{}.csv'.format(int(time.time()))

    connexion = sqlite3.connect(DATABASE_PATH)
    cursor = connexion.cursor()

    ### Create columns name
    cursor.execute('PRAGMA table_info(entreprises)')
    results = cursor.fetchall()

    line = ''

    for result in results:
        line = line + delimiter + result[1]
    
    lines.append(line.lstrip(delimiter))

    ### Get data
    cursor.execute('SELECT * FROM entreprises')
    results = cursor.fetchall()

    connexion.close()

    for result in results:
        line = ''
        for element in result:
            if element is None:
                element = ''

            line = line + delimiter + '"' + str(element) + '"'
        
        lines.append(line.lstrip(delimiter))

    csv_file = io.open(os.path.join('data', filename), 'w', encoding='utf8')
    
    for line in lines:
        csv_file.write(line + '\n')
    
    csv_file.close()