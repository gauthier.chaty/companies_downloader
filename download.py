import os
import io
import json

import requests


BASE_URL = 'https://entreprise.data.gouv.fr/api/sirene/v1/full_text'

def data_test():

    with io.open(os.path.join('data', 'test.json'), 'r', encoding='utf8') as infile:
        json_data = json.loads(infile.read())

    return json_data

def get_total_pages(letter, ape):
    
    url = BASE_URL + '/{letter}?per_page=100&activite_principale={ape}&page=1'.format(letter=letter, ape=ape)

    response = requests.get(url)
    if response.ok:
        json_data = response.json()
        return json_data['total_pages']

def get_data(letter, ape, page):

    url = BASE_URL + '/{letter}?per_page=100&activite_principale={ape}&page={page}'.format(letter=letter, ape=ape, page=page)
    response = requests.get(url)

    if response.ok:
        json_data = response.json()
        return json_data['etablissement']
        