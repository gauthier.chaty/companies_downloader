import argparse
import string

import database
import download

__VERSION__ = '0.1.0'

def update_data(companies_data):

    for data in companies_data:

        id_company = data['id']

        #print('Checking informations for {}'.format(id_company))

        if not database.check_if_exists(id_company):
            database.insert_company(data)


def get_companies_data(ape):

    for letter in string.ascii_lowercase:
        total_pages = download.get_total_pages(letter, ape)

        for page in range(1, total_pages+1):
            print('Downloading for {} : {} page {}/{}'.format(ape, letter.upper(), page, total_pages)) #DEBUG
            companies_data = download.get_data(letter, ape, page)
            update_data(companies_data)

def main():

    parser = argparse.ArgumentParser()

    parser.add_argument('ape', action='store', help='Code NAF to download')
    parser.add_argument('--export', action='store_true', help='Export to CSV')
    parser.add_argument('--version', action='version', version=__VERSION__)

    results = parser.parse_args()

    ape = results.ape
    export = results.export

    get_companies_data(ape)

    if export is not None:
        database.export_database(delimiter=';')
    
    
if __name__ == '__main__':
    main()